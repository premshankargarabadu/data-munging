from DataExtractor import *


class DataAnalyzer:
    def calculate_min_difference(self, data_list):
        data_list[1] = list(map(int, data_list[1]))
        data_list[2] = list(map(int, data_list[2]))

        result =[abs(data_list[1][i] - data_list[2][i]) for i in range(min(len(data_list[1]),len(data_list[2])))]
        minimum_value=(min(result))

        index_of_minimum_value = result.index(minimum_value)
        result = data_list[0][index_of_minimum_value]

        return minimum_value,result
