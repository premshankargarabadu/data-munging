class DataExtractor:
      def __init__(self,data_file,d):
         pass

      def extract_data(self,data_file,d,i,j,k):

        column0 = []
        column1 = []
        column2 = []
        with open(data_file, "r+") as f:
          data = f.readlines()
          # print(data)
          data1=[]
          for x in data:
            data1.append(x.replace(d,""))
          # print(data1)
          for line in data1:
            if line.strip():
              column0.append(line.strip().split()[int(i)])
              column1.append(line.strip().split()[int(j)])
              column2.append(line.strip().split()[int(k)])
        column0.pop(0)
        column1.pop(0)
        column2.pop(0)

        if d == "*":
          column0.pop()
          column1.pop()
          column2.pop()

        nested_listed = [column0,column1,column2]

        return nested_listed
