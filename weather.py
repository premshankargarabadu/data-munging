from DataAnalyzer import *

class Weather:
    def __init__(self, data_file_path,delimeter,i,j,k):
        self.data_file_path = data_file_path
        self.extractor = DataExtractor(self.data_file_path,delimeter)
        self.analyzer = DataAnalyzer()

    def calculate_min_difference(self):
        data_list = self.extractor.extract_data(self.data_file_path,d,i,j,k)
        return self.analyzer.calculate_min_difference(data_list)


if __name__ == "__main__":
    d= "*"
    i=0
    j=1
    k=2
    weather = Weather('weather.dat',d,i,j,k)
    result,result2 = weather.calculate_min_difference()
    print("The day number: ",result2)
    print("the smallest temperature spread: ",result)
