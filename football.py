from DataAnalyzer import *

class Football:
  def  __init__(self, data_file_path,delimeter,i,j,k):
    self.data_file_path = data_file_path
    self.extractor = DataExtractor(self.data_file_path,delimeter)
    self.analyzer = DataAnalyzer()


  def calculate_min_difference(self):
    data_list = self.extractor.extract_data(self.data_file_path,d,i,j,k)
    return self.analyzer.calculate_min_difference(data_list)




if __name__ == "__main__":
  d = "-"
  i=1
  j=6
  k=7
  football = Football('football.dat',d,i,j,k)
  result,result2 = football.calculate_min_difference()
  print("name of the team: ",result2)
  print("smallest difference in ‘for’ and ‘against’ goals.: ",result)

